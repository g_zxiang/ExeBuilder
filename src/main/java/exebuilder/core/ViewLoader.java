package exebuilder.core;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class ViewLoader {

  public static Parent load(String fxml) {
    Parent view = null;
    try {
      view = FXMLLoader.load(ViewLoader.class.getResource("/view/" + fxml + ".fxml"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return view;
  }
}
